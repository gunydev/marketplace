// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import VeeValidate from 'vee-validate'
Vue.use(VeeValidate)

import axios from 'axios' // HTTP client
window.axios = axios
import iziToast from 'iziToast' // Alert css
window.iziToast = iziToast
import swal from 'sweetalert2'
window.swal = swal

import VCalendar from 'v-calendar';
import 'v-calendar/lib/v-calendar.min.css';
// Use v-calendar, v-date-picker & v-popover components
Vue.use(VCalendar, {
  firstDayOfWeek: 1,  // Sunday
});

// Libraries
import Vuetify from 'vuetify'
// Helpers
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  theme: {
    primary: colors.green.accent4 // #00C853
  }
})
import('vuetify/dist/vuetify.min.css')

import VueSession from 'vue-session'
Vue.use(VueSession)

// filter
Vue.filter('nowDate', function() {
  let date = new Date().getDate()
  let month = new Date().getMonth()
  var months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ]
  let year = new Date().getFullYear()
  return `${date} ${months[month]} ${year}`
})

import firebase from 'firebase'
var config = {
  apiKey: 'AIzaSyCz4tmeYdaGvi68XVPrOg03QNaUjyFZvPg',
  authDomain: 'marketplace-f2b21.firebaseapp.com',
  databaseURL: 'https://marketplace-f2b21.firebaseio.com',
  projectId: 'marketplace-f2b21',
  storageBucket: 'marketplace-f2b21.appspot.com',
  messagingSenderId: '469874405165'
}
firebase.initializeApp(config)
Vue.use(firebase)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  data() {
    return {}
  },
  router,
  created() {
    // check device's width
    this.$nextTick(function() {
      console.log('device width: ' + window.innerWidth)
    })
    if (location.protocol != 'https:' && process.env.NODE_ENV != 'development') {
      location.href = 'https:' + window.location.href.substring(window.location.protocol.length)
    }
    if (!this.$session.exists()) {
      this.$router.push({ name: 'UserLogin' })
    }
  },
  components: { App },
  template: '<App/>'
})
