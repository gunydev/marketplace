export default {
  methods: {
    $_indexWhere(array, conditionFn) {
      const item = array.find(conditionFn)
      return array.indexOf(item)
    }
  }
}
