import Vue from 'vue'
import Router from 'vue-router'
const Dashboard = () => import('@/components/Dashboard')
const Seasonal = () => import('@/components/Seasonal')
const Purchase = () => import('@/components/orders/Purchase')
const Quotation = () => import('@/components/orders/Quotation')
const UserLogin = () => import('@/components/users/UserLogin')
const ProductInfo = () => import('@/components/ProductInfo')
const TheSearch = () => import('@/components/TheSearch')
const Productivity = () => import('@/components/Productivity')
const GroupCreate = () => import('@/components/groups/GroupCreate')
const Group = () => import('@/components/Group')
const UserProfile = () => import('@/components/users/UserProfile')
const Notifications = () => import('@/components/Notifications')
// Administrator
const PageAndPosition = () => import('@/components/admins/PageAndPosition')
const Role = () => import('@/components/admins/Role')
const Conversation = () => import('@/components/admins/Conversation')
const ImageAndColor = () => import('@/components/admins/ImageAndColor')

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Dashboard
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/seasonal',
      name: 'Seasonal',
      component: Seasonal
    },
    {
      path: '/quotation',
      name: 'Quotation',
      component: Quotation
    },
    {
      path: '/purchase',
      name: 'Purchase',
      component: Purchase
    },
    {
      path: '/login',
      name: 'UserLogin',
      component: UserLogin
    },
    {
      path: '/product/:mpproductid',
      name: 'ProductInfo',
      component: ProductInfo
    },
    {
      path: '/search/:productname',
      name: 'TheSearch',
      component: TheSearch
    },
    {
      path: '/productivity',
      name: 'Productivity',
      component: Productivity
    },
    {
      path: '/role',
      name: 'Role',
      component: Role
    },
    {
      path: '/group/create',
      name: 'GroupCreate',
      component: GroupCreate
    },
    {
      path: '/group/:groupid',
      name: 'Group',
      component: Group
    },
    {
      path: '/user/:userid',
      name: 'UserProfile',
      component: UserProfile
    },
    {
      path: '/notifications',
      name: 'Notifications',
      component: Notifications
    },
    {
      path: '/page-position',
      name: 'PageAndPosition',
      component: PageAndPosition
    },
    {
      path: '/conversation',
      name: 'Conversation',
      component: Conversation
    },
    {
      path: '/image-color',
      name: 'ImageAndColor',
      component: ImageAndColor
    }
  ],
  mode: 'history'
})
